var AWS = require('aws-sdk');
//AWS.config.update(config);
AWS.config.loadFromPath('./config.json');
var s3 = new AWS.S3();
var async = require("async");


Parse.Cloud.define('AddEmployee', function(req, res)
{
    var employee_id = req.params.id;
    var name = req.params.name;
    var joining_date = req.params.date;
    var emp_salary = req.params.salary;

   var Employee = Parse.Object.extend("Employee");
    var employee = new Employee();
    var date_var = new Date(joining_date);

    employee.set("Employee_Id", employee_id);
    employee.set("Employee_Name",name);
    employee.set("Joining_Date",date_var);
    employee.set("Salary",emp_salary);
    employee.save();
});

Parse.Cloud.define('GetEmployee', function(req,res){
    console.log("In Get Employee Function");

    var Employee = Parse.Object.extend("Employee");
    var query = new Parse.Query(Employee);

    console.log("Fetching data");
    console.log(query.find());
    return query.find();
});

Parse.Cloud.define('UpdateEmployee', function(req, res)
{
    console.log("In update function");

    var update_empid = Number(req.params.Employee_Id);
    var update_name = req.params.Employee_Name;
    var update_joining_date = req.params.Joining_Date;
    var update_emp_salary = Number(req.params.Salary);

    var updated_date = new Date(update_joining_date);

    var Employee = Parse.Object.extend("Employee");
    var query = new Parse.Query(Employee);

    query.get(req.params.objectId)
        .then(function(obj) {
        console.log("Update Query fired successfully.");
        console.log("id" + obj.id);
        obj.set("Employee_Id",update_empid);
        obj.set("Employee_Name",update_name);
        obj.set("Joining_Date",updated_date);
        obj.set("Salary",update_emp_salary);
        /*obj.set("__type",update_type);
        obj.set("className",update_class);
        obj.set("createdAt",update_createdAt);
        obj.set("updatedAt",update_updatedAt);*/
        obj.save().then(function(success){
            console.log("Updated successfully.");
        });
    },
    function(error){
        console.log(error);
    });
});


Parse.Cloud.define('DeleteEmployee', function(req, res) {
    console.log("In delete Employee function");
    var emp_id = req.params.objectId;

    var Employee = Parse.Object.extend("Employee");
    var query = new Parse.Query(Employee);

    query.equalTo("objectId", emp_id);
    query.first().then(function(obj){
            obj.destroy().then(function(response){
                    console.log("Object destroyed successfully");
                    console.log(response);
                    return response;
            },
            function(error){
                console.log(error);
                return error;
            });
        },
        function(err)
        {
            console.log("Query failed.");
            console.error(err);
        });
});



/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$                 STUDENT BACKEND          $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/


Parse.Cloud.define('GetStudent', function(req,res) {
    console.log("In Get Student Function");
    var Student = Parse.Object.extend("Student");
    var query = new Parse.Query(Student);
    return query.find();
});

Parse.Cloud.define('GetStudentdata', function(req,res) {
    console.log("In Get Student Function");
    var Student = Parse.Object.extend("Student");
    var query = new Parse.Query(Student);
    return query.find();
});

Parse.Cloud.define('DeleteStudent', function(req, res) {
    console.log("In delete Student function");
    var student_id = req.params.objectId;

    var Student = Parse.Object.extend("Student");
    var query = new Parse.Query(Student);

    query.equalTo("objectId", student_id);
    query.first().then(function(obj){
            obj.destroy().then(function(response){
                    console.log("Object destroyed successfully");
                    console.log(response);
                    return response;
                },
                function(error){
                    console.log(error);
                    return error;
                });
        },
        function(err)
        {
            console.log("Query failed.");
            console.error(err);
        });
});

Parse.Cloud.define('GetStudent_BetweenDates', function(req,res) {
    console.log("In GetStudent_BetweenDates Function");

    //var joining_date = req.params.DOB;
    var from_date = req.params.from_date;
    var to_date = req.params.to_date;

    var fromdate = new Date(from_date);
    var todate = new Date(to_date);

    var Student = Parse.Object.extend("Student");
    var query = new Parse.Query(Student);

    query.greaterThanOrEqualTo("DOB",fromdate);
    query.lessThanOrEqualTo("DOB",todate);

    return query.find();
});



/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/



var xmlParser = require('xml2json');
var xmlStr = require('fs');

Parse.Cloud.define('XMLtoJSON', function(req,res) {
    console.log("In XMLtoJSON Function");

    var start_date = new Date(req.params.start_date);
    var month = req.params.month;
    var creative_name = req.params.creative;
    console.log("recieved date is "+start_date);
    var date = new Date(start_date);
    var startdate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + (date.getDate());
    var monthcode = date.getUTCMonth() + 1;
    var lastDate = new Date(date.getFullYear(),(date.getMonth() + 1),0);
    var numOfDays =  lastDate.getDate()-date.getDate() + 1;
    var tempdate = new Date(startdate);

    var finalCount = [];

    var params = {
        Bucket: "seeme-dev-bucket",
        Delimiter: '/',
        Prefix: "Logs/System/HDBBBC/Novastar/2019-2"
    };
    var testData = {};
    var requiredTestData = [];
    var payload = {};
    payload.objects = [];

    var callback = function(err){
        console.log("Returning the results..");
        console.log(payload.list);
        console.log(payload.objects.length);
        return payload;
        //res.send(payload);
    }

    s3.listObjectsV2(params, function(err, data) {
        if(err){
            console.log(err, err.stack);
        }else{
            payload.list = data.Contents;

            async.each(data.Contents, function(file, cb){
                var getS3Params = {
                    Bucket: "seeme-dev-bucket",
                    Key: file.Key
                };
                var stream = s3.getObject(getS3Params, function(err, data){
                   payload.objects.push(data.Body.toString());
                    cb();
                });
            }, callback);
        }
    });
    /*s3.listObjectsV2(params,function(err, data){
       if(err)
       {
           console.log(err, err.stack);
       }
        else {
           console.log(data);
           testData = data;
           requiredTestData = testData.Contents;
           console.log(requiredTestData);
           console.log(requiredTestData.length);
           console.log(requiredTestData[0].Key);
           var getParams = {
               Bucket: "seeme-dev-bucket",
               Key: "Logs/System/HDBBBC/Novastar/2018-9-6-0.plylog"
           };
           s3.getObject(getParams, function(err, data){
               if(err){
                   console.log(err, err.stack);
               } else{
                   console.log("getData");
                   console.log(data);
                   console.log(data.Body);
               }
           });
       }
    });*/


    //console.log(requiredTestData[0].Key);
    //console.log(requiredTestData.length);
    //console.log(requiredTestData);

    /*for(var i = 0; i < numOfDays; i++) {
        console.log("date is "+startdate);
        if (xmlStr.existsSync('./Screen1/' + startdate + '.plylog')) {
            var str = xmlStr.readFileSync('./Screen1/' + startdate + '.plylog', 'utf8');
            if (str.length > 0) {
                var json_data = xmlParser.toJson(str);

                var data = JSON.parse(json_data);
                var distinct_data = data.PlayLog.PlayItem;

                var temp = [];
                var produce = {};

                for (var k = 0; k < distinct_data.length; k++) {
                    if (distinct_data[k].Name == creative_name) {
                        if (temp.indexOf(distinct_data[k].Name) == -1) {
                            temp.push(distinct_data[k].Name);
                            produce.Name = distinct_data[k].Name;
                            produce.Count = 1;
                            produce.Date = distinct_data[k].StartTime;
                        }
                        else {
                            var x = parseInt(produce.Count) + 1;
                            produce.Count = x;
                        }
                    }
                }
                console.log("count is:");
                console.log(produce);
                if (produce.Count) {
                    finalCount.push(produce);
                }
            }
        }
            tempdate.setDate(tempdate.getDate() + 1);
            startdate = tempdate.getUTCFullYear() + '-' + (tempdate.getUTCMonth() + 1) + '-' + (tempdate.getUTCDate() + 1);
            console.log("incremented date is : " + startdate);
    }
    return finalCount;*/
});
/* var lookup = {};
 var items = distinct_data;
 var result = [];

 for (var item, i = 0; item = items[i++];) {
 var name = item.Name;
 if (!(name in lookup)) {
 lookup[name] = 1;
 result.push(name);
 }
 }
 console.log("creative list is:");
 console.log(JSON.stringify(result));
 //return result;*/